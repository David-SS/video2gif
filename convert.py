import os
import glob
from pygifsicle import *
from moviepy.video.fx.resize import resize
from moviepy.video.fx.time_symmetrize import time_symmetrize
from moviepy.video.io.VideoFileClip import VideoFileClip


""" SCRIPT FUNCTIONS """


def optimize_gif(gif_path):
    gifsicle(
        sources=gif_path,
        optimize=True,
        colors=128,  # max possible of the source = 256
    )


def get_output_path(input_path, target_format, add_text=""):
    output_path = os.path.splitext(input_path)[0] + add_text + target_format
    print("\"{0}\" to \"{1}\"".format(input_path, output_path))
    return output_path


def convert_to_gif(input_path, target_format="gif", sym=False):
    # Get the output path
    output_path = get_output_path(input_path, target_format)

    # Get the clip from the video
    clip = VideoFileClip(input_path, audio=False)

    # Process stuff
    clip = clip.fx(resize, 0.3) if clip.size[0] > 600 else clip
    clip = clip.fx(time_symmetrize) if sym else clip

    # Final write gif
    clip.write_gif(output_path, fps=20, program='ffmpeg')  # 20 => decent for every normal gif

    # Return final file path
    return output_path


def compress_mp4(input_path, target_format="mp4", added_to_filename=""):
    # Get the output path
    output_path = get_output_path(input_path, target_format, added_to_filename)

    # Get the clip from the video
    video_clip = VideoFileClip(input_path, audio=False)

    # Final write video
    video_clip.write_videofile(output_path, preset='placebo')  # placebo = best compression

    # Return final file path
    return output_path


""" SCRIPT STARTS  """

# Conversion
print("Generating files: \n")

# CUSTOMIZE THIS ==>
final_gif_format = ".gif"
final_video_format = ".mp4"
formats_to_search = [".mp4", ".avi"]
just_rename_files_to_gif = True
take_compressed_video = False
added_to_filename = "_cz"
# CUSTOMIZE THIS <==

for input_format in formats_to_search:
    for file in [f for f in glob.glob("files/**/*" + input_format, recursive=True)]:
        compressed_video = compress_mp4(file, final_video_format, added_to_filename)

        if just_rename_files_to_gif:
            if not os.path.isfile(compressed_video):
                continue
            new_name = compressed_video.replace(os.path.splitext(compressed_video)[1], final_gif_format)
            os.rename(compressed_video, new_name)

            # Only delete original file if new one has a different name...
            if added_to_filename != "":
                os.remove(file)

        else:
            converted_gif = (
                convert_to_gif(compressed_video, final_gif_format, False) if take_compressed_video else
                convert_to_gif(file, final_gif_format, False)
            )
            optimize_gif(converted_gif)
            os.remove(file)


""""""""" 
IMPORTANT NOTES:
To make this works:
 - Use the virtual environment (with required libraries... you could install them manually)
 - Add gifsicle-1.92 folder in PATH [windows]
 - Copy directories/files you want to process in "files" folder
"""""""""
